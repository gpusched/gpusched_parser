class GPUTaskParams:

    ### GENERIC SECTION
    # RT params
    WCET                = "\tint wcet;\n";
    PERIOD              = "\tint period;\n";
    DEADLINE            = "\tint deadline;\n";


    # GPU params
    THREADS             = "\tint threads;\n";
    BLOCKS              = "\tint blocks;\n";
    SMEM                = "\tint smem;\n";
    REGS                = "\tint regs;\n";

    # General params
    NAME                = "\tstd::string kernel_name;\n"
    TASK_ID             = "\tint task_id;\n";

    # kernel can have different parameters types
    # use of variadic template here to be able to handle all of them
    FUNCTION            = "\tvoid (*f)(Types...);\n";
    # @TODO add here FUNCTION ARGUMENTS

   ### IMPL SECTION
    # RT params
    WCET_IMPL           = "\t.wcet       = {wcet},\n"
    PERIOD_IMPL         = "\t.period     = {period},\n"
    DEADLINE_IMPL       = "\t.deadline   = {deadline},\n"

    # GPU params
    THREADS_IMPL        = "\t.threads    = {threads},\n"
    BLOCKS_IMPL         = "\t.blocks     = {blocks},\n"
    SMEM_IMPL           = "\t.smem       = {smem},\n"
    REGS_IMPL           = "\t.regs       = {regs},\n"

    # GENERAL params
    TASK_ID_IMPL        = "\t.task_id    = {task_id},\n"
    FUNCTION_IMPL       = "\t.{name}=&{kernel_name},\n" # remove this never call it
    # @TODO add here FUNCTION ARGUMENTS
    NAME_IMPL           = "\t.kernel_name = std::string(\"{name}\")\n"

    #Parse Index
    TYPE_IDX        = 0
    ID_IDX          = 1
    WCET_IDX        = 2
    PERIOD_IDX      = 4
    DEADLINE_IDX    = 5
    BLOCKS_IDX      = 7
    THREADS_IDX     = 8
    REGS_IDX        = 9
    SMEM_IDX        = 10
    NAME_IDX        = 11

    # STRUCT NAME
    STRUCT_NAME     = "gpusched_task_t"
    WRAPPER_STRUCT_NAME = "gpusched_wrapper_task_t"
    def dump_struct():

        #dump =  "template<typename ...Types>\n"
        #dump += "struct {struct_name}: public {wrapper_name} {{\n".format(struct_name=GPUTaskParams.STRUCT_NAME, wrapper_name=GPUTaskParams.WRAPPER_STRUCT_NAME)
        dump = "struct {struct_name}{{\n".format(struct_name=GPUTaskParams.STRUCT_NAME)
        dump = dump + GPUTaskParams.WCET
        dump = dump + GPUTaskParams.PERIOD
        dump = dump + GPUTaskParams.DEADLINE
        dump = dump + "\t// gpu attributes\n"
        dump = dump + GPUTaskParams.BLOCKS
        dump = dump + GPUTaskParams.THREADS
        dump = dump + GPUTaskParams.REGS
        dump = dump + GPUTaskParams.SMEM
        dump = dump + "\t// general attributes\n"
        dump = dump + GPUTaskParams.TASK_ID
        #dump = dump + GPUTaskParams.FUNCTION
        dump = dump + GPUTaskParams.NAME
        dump = dump + "};\n"
        return dump

    def dump_struct_wrapper():

        dump = "struct {wrapper_name}{{}};\n\n".format(wrapper_name=GPUTaskParams.WRAPPER_STRUCT_NAME)
        return dump
    # global vector is useless for now
    def dump_global_vector():
        dump =  "static std::vector<std::shared_ptr<{wrapper_name}> >& get_gpusched_task_vector() {{\n".format(wrapper_name=GPUTaskParams.WRAPPER_STRUCT_NAME)
        dump += "\tstd::vector<std:::shared_ptr<{wrapper_name}> > gpu_tasks;\n".format(wrapper_name=GPUTaskParams.WRAPPER_STRUCT_NAME)
        for task in range(0, GPUTask.NUMBER_OF_TASKS):
            dump += "\tgpu_tasks.push_back(std::shared_ptr<{wrapper_name}>(task_{task_no}));\n".format(wrapper_name=GPUTaskParams.WRAPPER_STRUCT_NAME,task_no=task)
        dump += "}\n"

        return dump
#@TODO store the cuda kernel definition directly in the structure so we can call it with its params.
class GPUTask:

    NUMBER_OF_TASKS = 0

    def __init__(self, task_id, wcet, period, deadline, blocks, threads, regs, smem, name, kfile):

        GPUTask.NUMBER_OF_TASKS += 1
        print("Kernel [{name}] found".format(name=name))
        # rt params
        self.task_id    = task_id
        self.wcet       = wcet
        self.period     = period
        self.deadline   = deadline

        # gpu params
        self.threads    = threads
        self.blocks     = blocks
        self.smem       = smem
        self.regs       = regs

        # general params
        self.name       = name
        self.task_id    = task_id

        #specific parameters to retrieve the cuda kernel declarations
        self.kfile      = kfile

        self.kernel_parameters_variable = []
        self.init_function_name = ""

    def dump_str(self) -> str:
        dump = "struct {struct_name} task_{no} {{\n".format(struct_name=GPUTaskParams.STRUCT_NAME, no=self.task_id)

        #@TODO add a field to store the cuda kernel pointer
        dump = dump + "\t// RT attributes\n"
        dump = dump + GPUTaskParams.WCET_IMPL.format(wcet=self.wcet)
        dump = dump + GPUTaskParams.PERIOD_IMPL.format(period=self.period)
        dump = dump + GPUTaskParams.DEADLINE_IMPL.format(deadline=self.deadline)
        dump = dump + "\t// gpu attributes\n"
        dump = dump + GPUTaskParams.BLOCKS_IMPL.format(blocks=self.blocks)
        dump = dump + GPUTaskParams.THREADS_IMPL.format(threads=self.threads)
        dump = dump + GPUTaskParams.REGS_IMPL.format(regs=self.regs)
        dump = dump + GPUTaskParams.SMEM_IMPL.format(smem=self.smem)
        dump = dump + "\t// general attributes\n"
        dump = dump + GPUTaskParams.TASK_ID_IMPL.format(task_id=self.task_id)
        #dump = dump + GPUTaskParams.FUNCTION_IMPL.format(name='f', kernel_name = self.name, params=self.get_kernel_declaration())
        dump = dump + GPUTaskParams.NAME_IMPL.format(name=self.name)
        dump = dump + "};\n"

        self.get_variables_declaration()
        return dump

    def get_kernel_declaration(self) -> str:
        self.get_variables_declaration()
        print(self.kernel_parameters_variable)
        print(self.init_function_name)
        with open(self.kfile, 'r') as inFile:

            inFileLines = inFile.readlines()

            for line in inFileLines:
                if "__global__" in line and self.name in line:
                    kernel_arguments = line.split(self.name)[1].strip()
                    if(kernel_arguments[-1] == ')'):
                        return kernel_arguments
                    else:
                        while(kernel_arguments[-1] != ')'):
                            kernel_arguments = kernel_arguments[:-1]
                        return kernel_arguments

                if line == '-1\n':
                    break
            return kernel_arguments
    def get_variables_declaration(self):
        parameters = []
        kernel_detected = False
        parameters_detected = False
        init_name = ""
        with open(self.kfile, 'r') as inFile:
            inFileLines = inFile.readlines()
            for idx,line in enumerate(inFileLines):
                if self.name in line and "Start" in line:
                    kernel_detected = True
                if self.name in line and "End" in line:
                    flag_kernel_detected = False
                    break

                if kernel_detected and "Start Parameters" in line:
                    parameters_detected = True
                elif "End Parameters" in line:
                    parameters_detected = False
                elif parameters_detected and not line.isspace():
                    parameters.append(line)

                if kernel_detected and "Start Init function" in line:
                    init_name = inFileLines[idx+1]
                    break
        for parameter in parameters:
            parameter = (parameter.split(" ")[-1]).rstrip(';\n')
            self.kernel_parameters_variable.append(parameter)
        self.init_function_name = init_name.split("void")[1].lstrip().rstrip(" {\n")
