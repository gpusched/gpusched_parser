
class GPUSchedStepParams:
    # Global Struct params
    ARRAY               ="\tconst int * jobs_array;\n"
    STEP_NO             ="\tint step_no;\n"
    TIME                ="\tint time;\n"
    NUMBER_KERNELS      ="\tint number_kernel;\n"
    MINIMAL_START_TIME  ="\tint minimal_time_start;\n"

    # Impl struct params
    ARRAY_IMPL              ="\t.jobs_array = ((const int[{SIZE}]){{{list_jobs}}}),\n"
    STEP_NO_IMPL            ="\t.step_no = {step_no},\n"
    TIME_IMPL               ="\t.time   = 0,\n"
    NUMBER_KERNELS_IMPL     ="\t.number_kernel = {number_kernel},\n"
    MINIMAL_START_TIME_IMPL ="\t.minimal_time_start = {minimal_start}\n"

    # Struct name
    STRUCT_NAME = "gpusched_step_t"

    LIST_JOBS_OBJECT = []
    def dump_struct():

        dump = "struct gpusched_step_t {\n"
        dump += GPUSchedStepParams.ARRAY
        dump += GPUSchedStepParams.STEP_NO
        dump += GPUSchedStepParams.TIME
        dump += GPUSchedStepParams.NUMBER_KERNELS
        dump += GPUSchedStepParams.MINIMAL_START_TIME
        dump += "};\n\n"
        return dump

    def dump_global_vector():
        dump =  "static std::vector<{struct_name}>& get_gpusched_steps()\n{{\n".format(struct_name=GPUSchedStepParams.STRUCT_NAME)
        dump += "\tstatic std::vector<{struct_name}> steps;\n".format(struct_name=GPUSchedStepParams.STRUCT_NAME)
        dump += "\t// Add all steps in vector\n"
        for i in range(0, GPUSchedResultStep.NUMBER_STEP+1):
            dump += "\tsteps.push_back(step_{no});\n".format(no=i)
        dump += "\treturn steps;\n}\n\n"

        return dump
class GPUSchedResultStep:

    STRUCT_STEP = "gpusched_step_t step_{no} = {{\n";

    TRACK_MAX_CONCURRENT_JOBS = 0 # tracker to know how much stream is required for this schedule order

    NUMBER_STEP = 0

    def __init__(self, number_kernels, list_jobs, step_no):

        if GPUSchedResultStep.TRACK_MAX_CONCURRENT_JOBS < number_kernels:
            GPUSchedResultStep.TRACK_MAX_CONCURRENT_JOBS = number_kernels

        GPUSchedResultStep.NUMBER_STEP = step_no

        self.number_kernels = number_kernels # number of kernels to execute for this step
        self.list_jobs      = list_jobs # list of jobs
        self.step_no        = step_no
        # differentiate case when we only have one kernel we don't want to join with comma
        if number_kernels != 1:
            self.str_list_jobs  = ','.join(self.list_jobs)
        else:
            self.str_list_jobs = str(list_jobs)
        self.min_time_start = get_max_job_arrival_time(self.list_jobs)
        self.dump_str()

    def dump_str(self):

        dump =  GPUSchedResultStep.STRUCT_STEP.format(no=self.step_no)
        dump += GPUSchedStepParams.ARRAY_IMPL.format(SIZE=self.number_kernels, list_jobs = self.str_list_jobs)
        dump += GPUSchedStepParams.STEP_NO_IMPL.format(step_no=self.step_no)
        dump += GPUSchedStepParams.TIME_IMPL
        dump += GPUSchedStepParams.NUMBER_KERNELS_IMPL.format(number_kernel=self.number_kernels)
        #dump += GPUSchedStepParams.MINIMAL_START_TIME_IMPL.format(minimal_start=get_max_job_arrival_time(self.list_jobs))
        dump += GPUSchedStepParams.MINIMAL_START_TIME_IMPL.format(minimal_start=self.min_time_start)
        dump += "};\n\n";
        return dump;

"""
    Retrieve the job within a list of jobs with the maximal arrival time

    @param:
    jobs        list of jobs
"""
def get_max_job_arrival_time(jobs):
    max_time = -1
    if type(jobs) == str:
        max_time = int(GPUSchedStepParams.LIST_JOBS_OBJECT[int(jobs, 10)].arrival_time, 10)
    else:
        for job_id in jobs:
            curr_arrival_time = int(GPUSchedStepParams.LIST_JOBS_OBJECT[int(job_id,10)].arrival_time,10)
            if max_time < curr_arrival_time:
                max_time = curr_arrival_time
    return max_time
