
class GPUJobParams:
    # RT params
    ARRIVAL_TIME        = "\tint arr_time    = {arr_time};\n"
    RELATIVE_DEADLINE   = "\tint relative_dl = {relative_dl};\n "

    # GENERAL params
    RELATED_TASK        = "\tstruct {kernel_name}_t  rl_task = {related_task};\n"
    JOB_ID              = "\tint job_id       = {job_id};\n"

    RELATED_TASK_NAME   = "\tstd::string rlt_name = std::string(\"{name}\");\n"

    # Parse Index
    # Jobs file structure:
    # job_id    related_task_id     arr_time    rl_deadline
    ID_IDX                  = 0
    RELATED_TASK_IDX        = 1
    ARR_TIME_IDX            = 2
    RELATIVE_DEADLINE_IDX   = 3


class GPUJob:

    def __init__(self, job_id, related_task_id, arr_time, relative_deadline):

        # rt params
        self.arrival_time       = arr_time
        self.relative_deadline  = relative_deadline

        # general params
        self.related_task_id    = related_task_id
        self.job_id             = job_id
