import argparse # arguments parser
import os       # getcwd
import re       # Regex

from src.gputask.gputask import GPUTaskParams, GPUTask
from src.gpujob.gpujob import GPUJobParams, GPUJob
from src.gpusched_result.gpusched_result import GPUSchedResultStep, GPUSchedStepParams

relation_job_id_task_id    = dict()
relation_task_id_task_name = dict()

list_gpu_tasks_object = []
list_gpusched_step_object = []
list_gpu_jobs_object = []

"""
    Parse a file to retrieve GPU task information.

    Expect file format:
    task_type   id  wcet    bcet    period  deadline    -1  block   thread  regs    smems   name

    @param:
    GPUTasksFile    path to gpu tasks file
"""
def parse_tasks(args):

    gpuTasksCount = 0
    print(os.getcwd())
    with open(args.gpu_tasks, "r") as inFile, open("gpu_tasks.hpp", "w") as outFile:

        inFileLines = inFile.readlines()
        print("Generate GPU tasks file: gpu_tasks.hpp")

        outFile.write("#ifndef __GPU_TASKS_HPP\n"
                      "#define __GPU_TASKS_HPP\n"
                      "#include <memory>\n"
                      "#include <vector>\n"
                      "#include \"{k_dec}\"\n".format(k_dec=args.kernel_declaration)
                      )

        # dump the wrapper for vector
        #outFile.write(GPUTaskParams.dump_struct_wrapper())
        # dump the gputask structure here
        outFile.write(GPUTaskParams.dump_struct())
        for line in inFileLines:
            task_attributes = line.split(" ")
            gpuTasksCount += 1
            if line == '-1\n':
                break
            # check if the current task is a gpu task
            if task_attributes[0] != '1':
                pass
            currTask = GPUTask(task_attributes[GPUTaskParams.ID_IDX],
                               task_attributes[GPUTaskParams.WCET_IDX],
                               task_attributes[GPUTaskParams.PERIOD_IDX],
                               task_attributes[GPUTaskParams.DEADLINE_IDX],
                               task_attributes[GPUTaskParams.BLOCKS_IDX],
                               task_attributes[GPUTaskParams.THREADS_IDX],
                               task_attributes[GPUTaskParams.REGS_IDX],
                               task_attributes[GPUTaskParams.SMEM_IDX],
                               task_attributes[GPUTaskParams.NAME_IDX].rstrip('\n'),
                               args.kernel_declaration)

            list_gpu_tasks_object.append(currTask)
            outFile.write(currTask.dump_str() + "\n")
            relation_task_id_task_name[currTask.task_id] = currTask.name
        #outFile.write(GPUTaskParams.dump_global_vector())
        outFile.write("#endif")

        print("gpu_tasks.hpp file generated")

"""
    Parse a file to retrieve GPU task information.

    Expect file format:
    task_type   id  wcet    bcet    period  deadline    -1  block   thread  regs    smems   name

    @param:
    GPUTasksFile    path to gpu tasks file
"""
def parse_jobs(args):

    gpuJobsCount = 0
    print(os.getcwd())
    with open(args.gpu_jobs, "r") as inFile, open(args.output_file, "a") as outFile:
        inFileLines = inFile.readlines()

        for line in inFileLines:
            job_attributes = line.split(" ")
            gpuJobsCount += 1

            # check if the current task is a gpu task
            currJob = GPUJob(job_attributes[GPUJobParams.ID_IDX],
                             job_attributes[GPUJobParams.RELATED_TASK_IDX],
                             job_attributes[GPUJobParams.ARR_TIME_IDX],
                             job_attributes[GPUJobParams.RELATIVE_DEADLINE_IDX]
                             )
            relation_job_id_task_id[currJob.job_id] = currJob.related_task_id
            list_gpu_jobs_object.append(currJob)
    print(relation_job_id_task_id)
    GPUSchedStepParams.LIST_JOBS_OBJECT = list_gpu_jobs_object


def parse_sched_order(args):
    steps = 0
    token = ["|", "[", "]", "-->"]
    step_no = 0
    with open(args.schedule, "r") as inFile, open("steps_struct.hpp", "w") as outFile:

        schedOrder = inFile.readline()
        schedOrder = schedOrder.split('-->')
        steps = len(schedOrder)
        print("Schedule has {} steps".format(steps))
        outFile.write("#ifndef __GPUSCHED_STEPS__HPP\n")
        outFile.write("#define __GPUSCHED_STEPS__HPP\n")
        outFile.write("#include <vector>\n")
        outFile.write(GPUSchedStepParams.dump_struct())
        for step in schedOrder:
            step = step.rstrip(' ').lstrip(' ').rstrip('\n')
            # If to differentiate step with multiple kernels call and those with only one
            if(step[0] ==  token[1] and step[-1] == token[2]):
                step = step[1:-1].split(token[0])
                number_kernels = len(step)
            else:
                number_kernels = 1
            currGpuSchedStep = GPUSchedResultStep(number_kernels, step, step_no)
            outFile.write(currGpuSchedStep.dump_str())

            list_gpusched_step_object.append(currGpuSchedStep)
            step_no += 1

        outFile.write(GPUSchedStepParams.dump_global_vector())
        outFile.write("#endif")

"""
    Retrieve a task in list_gpu_tasks_object by its id.

    @param
    task_id     id we search
"""
def get_task_by_id(task_id):
    for task in list_gpu_tasks_object:
        # GpuTask id is store as str
        if int(task.task_id,10) == task_id:
            return task
    return None
